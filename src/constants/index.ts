export const IS_PROD = process.env.node === 'production';
// api Prefix
export const BASE_API_TEST = 'https://gorest.co.in/public-api';
export const BASE_API_URL = 'https://obscure-shore-42367.herokuapp.com/api';

export const COMPANY_NAME = 'YBizTech';
export const ICON_FONT_URL = '//at.alicdn.com/t/font_2221049_8szkpgxzd38.js';
