export const ROUTE = {
  login: '/auth/login',
  home: '/',
  forgotPassword: '/auth/forgot-password',
  register: '/auth/register',
  userManagement: '/user-management',
  supplierManagement: '/supplier-management',
  stockManagement: '/stock-management',
  saleManagement: '/sale-management',
  accountManagement: '/account-management',
  reportManagement: '/report-management',
};
